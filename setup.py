#!./venv/bin/python

from setuptools import setup, find_packages


setup(
    name='mapper',
    version='0.0.2',
    description='',
    packages=find_packages('src',
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    package_dir={'': 'src'},
    author='7Segments',
    py_modules=[''],
    install_requires=[
    ],
    tests_require=['nose==1.3.0', 'PyHamcrest==1.7.1', 'mock==1.0.1'],
)
