from collections import namedtuple
from unittest import TestCase
from hamcrest import assert_that, is_
from mapper.fields import StringField, TypeField
from mapper.models import Model

IdField1 = namedtuple('IdField1', 'id')
IdField2 = namedtuple('IdField2', 'id')
IdField3 = namedtuple('IdField3', 'id')


class MyObject2(Model):
    id = TypeField([IdField1, IdField2])

class MyObject(Model):
    _field1 = StringField()

    def __init__(self, value):
        self._field1 = value

    @property
    def value(self):
        return self._field1

    @value.setter
    def value(self, value):
        self._field1 = value




class FieldTest(TestCase):
    def test_extract_name(self):
        assert_that(MyObject._field1.field_name, is_('_field1'))

    def test_set_get(self):
        obj = MyObject('v1')

        assert_that(obj.value, is_('v1'))

        obj.value = 'v2'
        assert_that(obj.value, is_('v2'))

    def test_validate_type(self):
        obj = MyObject2()
        obj.id = IdField1('1')
        obj.id = IdField2('2')

        try:
            obj.id = IdField3('3')
            self.fail('IdField3 should not be assignable')
        except ValueError:
            pass

    def test_multiple_instances(self):
        obj = MyObject2()
        obj.id = IdField1('1')
        assert_that(obj.id, is_(IdField1('1')))

        obj2 = MyObject2()
        assert_that(obj2.id, is_(None))

