from collections import namedtuple
from unittest import TestCase
from hamcrest import assert_that, is_, has_properties
from mapper import Mapping, Field, PropertyField, InheritedField
from mapper.fields import TypeField, StringField
from mapper.models import Model
from mapper.nosql import NamedTupleField


class Campaign(Model):
    id = StringField()

    def __init__(self, id, message):
        self.id = id
        self.message = message


EmailMessage = namedtuple('EmailMessage', 'subject message')


class EmailCampaign(Campaign):
    message = TypeField(EmailMessage)

    @property
    def channel(self):
        return 'email'


class SmsCampaign(Campaign):
    message = StringField()

    @property
    def channel(self):
        return 'sms'


def campaign_factory(json):
    cls = {'email': EmailCampaign, 'sms': SmsCampaign}[json['channel']]
    return cls.__new__(cls)


campaign_mapping = Mapping(
    campaign_factory,
    [
        Field(Campaign.id, 'id'),
        PropertyField('channel', 'channel'),
        InheritedField(
            'channel',
            'message',
            'message_s',
            email=NamedTupleField(EmailCampaign.message),
            sms=Field(SmsCampaign.message),
        )

    ]
)


class MappingTest(TestCase):
    def test_inheritance_to_json(self):
        a = EmailCampaign(1, EmailMessage('subject', 'email message'))
        b = SmsCampaign(2, 'sms message')

        assert_that(campaign_mapping.to_json(a), is_(
            {
                'id': 1,
                'channel': 'email',
                'message_s': {'subject': 'subject', 'message': 'email message'}
            }
        ))

        assert_that(campaign_mapping.to_json(b), is_(
            {
                'id': 2,
                'channel': 'sms',
                'message_s': 'sms message'
            }
        ))

    def test_inheritance_from_json_email(self):
        a = campaign_mapping.from_json({
            'id': 1, 'channel': 'email',
            'message_s': {
                'subject': 'subject',
                'message': 'email message'
            }
        })

        assert_that(a, has_properties(
            message=EmailMessage('subject', 'email message'), id=1,
            channel='email'))

    def test_inheritance_from_json_sms(self):
        a = campaign_mapping.from_json({
            'id': 2,
            'channel': 'sms',
            'message_s': 'sms message'
        }
        )

        assert_that(a, has_properties(message='sms message', id=2, channel='sms'))