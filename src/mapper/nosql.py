class Mapping(object):
    def __init__(self, target_class, fields):
        self._fields = fields
        self._target_class = target_class
        self._field_mapping = {a.target: a for a in self._fields if a.target}

    def __getitem__(self, item):
        return self._field_mapping[item]

    def to_json(self, obj):
        return dict([field.to_json(obj) for field in self._fields])

    def from_json(self, json):
        if json:
            target = self._target_class(json)
            for field in self._fields:
                field.set_value(target, json)
            return target


class AbstractField(object):
    @property
    def target(self):
        raise NotImplementedError()

    def get_value(self, obj):
        raise NotImplementedError()

    def to_json(self, obj):
        raise NotImplementedError()

    def value_to_json(self, value):
        raise NotImplementedError()

    def find(self, value):
        raise NotImplementedError()

    def from_json(self, json):
        raise NotImplementedError()

    def set_value(self, target, json):
        raise NotImplementedError('set_value is not implemented in %s' % self.__class__)


class Field(AbstractField):
    def __init__(self, field, target=None):
        self._field = field
        self._target = target

    @property
    def target(self):
        return self._target

    def get_value(self, obj):
        return self._field.__get__(obj, obj.__class__)

    def to_json(self, obj):
        return self._target, self.value_to_json(self.get_value(obj))

    def value_to_json(self, value):
        return value

    def find(self, value):
        return {self._target: self.value_to_json(value)}

    def from_json(self, json):
        return json[self._target]

    def set_value(self, target, json):
        self._field.__set__(target, self.from_json(json))


class NamedTupleField(Field):
    def value_to_json(self, value):
        return value._asdict() if value  else None

    def from_value(self, json):
        if json:
            return self._field.target_class(**json)

    def from_json(self, json):
        return self.from_value(json[self._target])

class ReferenceField(Field):
    def value_to_json(self, value):
        return '|'.join(map(str, value)) if value  else None

    def from_value(self, json):
        if json:
            return self._field.target_class(*json.split('|'))

    def from_json(self, json):
        return self.from_value(json[self._target])


class ReadOnlyField(Field):
    def to_json(self, obj):
        value = obj
        for p in (self._field if isinstance(self._field, tuple) else [self._field]):
            value = getattr(value, p)

        return self._target, value

    def set_value(self, target, json):
        pass


class ListField(Field):
    def to_json(self, obj):
        values = self._field.get_value(obj)
        return self._target, map(self._field.value_to_json, values)


    def set_value(self, target, json):
        value = map(self._field.from_value, json[self._target])
        self._field._field.__set__(target, value)


class PropertyField(Field):
    def to_json(self, obj):
        return self.target, getattr(obj, self._field)

    def set_value(self, target, json):
        pass


class InheritedField(AbstractField):
    def __init__(self, choice_field, field, target, **choice_mappings):
        self._choice_field = choice_field
        self._target = target
        self._field = field
        self._choice_mappings = choice_mappings

        for mapping in self._choice_mappings.values():
            mapping._target = target


    @property
    def target(self):
        return self._target

    def _mapping_target(self, obj):
        choice_value = getattr(obj, self._choice_field)
        return self._choice_mappings[choice_value]

    def to_json(self, obj):
        return self._mapping_target(obj).to_json(obj)

    def _mapping_value_target(self, json):
        mapping_target = self._choice_mappings[json[self._choice_field]]
        return mapping_target

    def set_value(self, target, json):
        return self._mapping_value_target(json).set_value(target, json)


