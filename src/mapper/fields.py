import datetime


class Field(object):
    def validate(self, instance, value):
        for validator in self._validators:
            validator(instance, value)

    @property
    def _default(self):
        return None

    def __init__(self, validators=None):
        self._validators = validators or []

    def __set__(self, instance, value):
        self.validate(instance, value)

        self._initialize(instance)

        instance._values[self._name] = value

    def _initialize(self, instance):
        if not hasattr(instance, '_values'):
            instance._values = {self._name: self._default}

    def __get__(self, instance, owner):
        if instance is None:
            return self

        self._initialize(instance)

        return instance._values.setdefault(self._name, self._default)

    def contribute_to_class(self, name):
        self._name = name

    @property
    def field_name(self):
        return self._name


class StringField(Field):
    pass


class TypeField(Field):
    '''
    TypeField validates value to be of target class. Target class can be either
    single class or list of classes.
    '''

    def __init__(self, target_class, validators=None):
        validators = validators or []
        validators.insert(0, self._validate_type)
        super(TypeField, self).__init__(validators=validators)

        self._target_class = target_class

    @property
    def target_class(self):
        return self._target_classes[0]

    @property
    def _target_classes(self):
        return self._target_class if isinstance(self._target_class, list) else [
            self._target_class]

    def _validate_type(self, instance, value):
        validate_class(self._target_classes, value)


class ListField(Field):
    def __init__(self, target_class, validators=None):
        validators = validators or []
        validators.insert(0, self._validate_type)
        super(ListField, self).__init__(validators=validators)

        self._target_class = target_class

    @property
    def target_class(self):
        return self._target_class

    @property
    def _default(self):
        return []

    def _validate_type(self, instance, value):
        if not isinstance(value, list):
            raise ValueError('%r should be instance of list' % (value,))

        for v in value:
            validate_class(self._target_class, v)


class DictField(Field):
    def __init__(self, validators=None):
        super(DictField, self).__init__(validators=validators)

    @property
    def _default(self):
        return {}


    def _validate_type(self, instance, value):
        if not isinstance(value, dict):
            raise ValueError(
                '%r should be instance of dict' % value)


def validate_class(target_classes, value):
    target_classes = target_classes if isinstance(target_classes, list) else [target_classes]

    validate_target_classes = (isinstance(value, target_class) for
                               target_class in target_classes)
    if value is not None and not any(validate_target_classes):
        raise ValueError('%r is not of type %s' % (
            value, ', '.join(map(repr, target_classes))))


class DatetimeField(TypeField):
    def __init__(self, validators=None):
        super(DatetimeField, self).__init__(datetime.datetime, validators=validators)
