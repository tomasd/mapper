from mapper.fields import Field


class ModelMeta(type):
    def __new__(cls, name, bases, attrs):
        for key, value in attrs.iteritems():
            if isinstance(value, Field):
                value.contribute_to_class(key)

        klass = type.__new__(cls, name, bases, attrs)
        return klass


class Model(object):
    __metaclass__ = ModelMeta